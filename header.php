<?php $rand=rand(0,10000);?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>CGAFRICA</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-switch.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/custom.css" rel="stylesheet">
<link href="css/popup.css" rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->

<link rel="icon" href="images/favicon.png" type="image/png" />
<link rel="shortcut icon" href="images/favicon.png" type="image/png" />
</head>

<body id="body">
<div class="" style="background:#F3F3F3">
<div class="container">
  <div class="row">
    <div class="col-sm-12">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="images/logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Featured</a></li>
        <li><a href="#">Gallery</a></li>
        <li><a href="#">Jobs</a></li>
        <li><a href="#">Forums</a></li>
      </ul>
      
<ul class="nav navbar-nav navbar-right">
      <li><a href="#"><i class="fa fa-plus"></i> Upload Image</a></li>
	  <li><a href="#"><img src="images/email.png"></a></li>
	  <li><a href="#"><img src="images/notifications.png"></a></li>
	  <li><a onClick="$('#light_signin,#fade').show();"  href="javascript:void(0);">Sign In</a></li>
	  <li><a onClick="$('#light_signup,#fade').show();"  href="javascript:void(0);"><img style="width:46px;float:left;margin-right:10px;margin-top:-15px;" src="images/profile_image.png"> Sign Up</a></li>
<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img style="width:46px;float:left;margin-right:10px;margin-top:-15px;" src="images/profile_image.png"> Redwan Rahman
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">My Profile</a></li>
          <li><a href="#">Upload Image</a></li>
          <li><a href="#">Email Subscription</a></li>
		  <li><a href="#">Logout</a></li>
        </ul>
      </li>
	  <li><a href="#" data-toggle="dropdown"><img src="images/search.png"></a>
	  <form action="" class="dropdown-menu" style="padding:0;border-radius:0;width:280px;">
	  <input type="text" style="border: 0px none; background: rgb(241, 241, 241) none repeat scroll 0% 0%; width: 100%; font-size: 16px; padding: 8px 10px;" placeholder="Search CG Africa" name="Search">
	  </form>
	  </li>
	  <li><a href="#" data-toggle="dropdown"><img src="images/mobile.png"></a>
	  <ul class="dropdown-menu">
          <li><a href="#">Home</a></li>
          <li><a href="#">Gallery</a></li>
          <li><a href="#">Featured</a></li>
		  <li><a href="#">Logout</a></li>
        </ul>
	  </li>
	  
          </ul>
          
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
	</div>
    
  </div>
  
</div>
</div>


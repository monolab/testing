<?php include_once('header.php');?>
<div class="container">
<div class="row page">
<div class="col-sm-6"><h1>Meet the Team</h1></div>
<div class="col-sm-12">
<div class="titem" style="margin-top:20px;"><img style="padding:30px 35px; background:red;border-radius:50%; float:left;margin-right:20px;" src="images/profile.png"> <p style="line-height:18px;padding-top:20px;"><strong style="font-family:segoeuisb;font-size:33px;">Mayowa Osewa</strong><br><span style="font-family:segoeuil;font-size:13px;">Director/ UX Designer</span><br><span style="font-family:segoeuil;font-size:13px;">Contact me | Follow me</span></p></div>
<p style="margin-left:100px;padding-bottom:40px;border-bottom:solid 1px #ccc;">After training as an Architect for 5 years, I then subsequently proceeded to a Master’s 
Degree in ‘Built Environment’ from the University of Greenwich. I have a strong affinity for 
both digital and physical designs. Starting my 3D journey with 3ds Max in 2002, I ‘played’ 
with other related tools such as Maya and Combustion (before its demise). I have since 
worked as an Architectural Designer, Visualisation Artist and Branding & Design Developer
As a believer in the role of effective communication between the designer and client, 
I have dedicated a considerable amount of time to making known the importance of 
visualisation in the design field, especially in architecture.</p>
<div class="titem" style="margin-top:20px;"><img style="padding:30px 35px; background:red;border-radius:50%; float:left;margin-right:20px;" src="images/profile.png"> <p style="line-height:18px;padding-top:20px;"><strong style="font-family:segoeuisb;font-size:33px;">Femi Osewa</strong><br><span style="font-family:segoeuil;font-size:13px;">Director/ UX Designer</span><br><span style="font-family:segoeuil;font-size:13px;">Contact me | Follow me</span></p></div>
<p style="margin-left:100px;padding-bottom:40px;border-bottom:solid 1px #ccc;">After training as an Architect for 5 years, I then subsequently proceeded to a Master’s 
Degree in ‘Built Environment’ from the University of Greenwich. I have a strong affinity for 
both digital and physical designs. Starting my 3D journey with 3ds Max in 2002, I ‘played’ 
with other related tools such as Maya and Combustion (before its demise). I have since 
worked as an Architectural Designer, Visualisation Artist and Branding & Design Developer
As a believer in the role of effective communication between the designer and client, 
I have dedicated a considerable amount of time to making known the importance of 
visualisation in the design field, especially in architecture.</p>
<div class="titem" style="margin-top:20px;"><img style="padding:30px 35px; background:red;border-radius:50%; float:left;margin-right:20px;" src="images/profile.png"> <p style="line-height:18px;padding-top:20px;"><strong style="font-family:segoeuisb;font-size:33px;">Femi Osewa</strong><br><span style="font-family:segoeuil;font-size:13px;">Director/ UX Designer</span><br><span style="font-family:segoeuil;font-size:13px;">Contact me | Follow me</span></p></div>
<p style="margin-left:100px;padding-bottom:40px;border-bottom:solid 1px #ccc;">After training as an Architect for 5 years, I then subsequently proceeded to a Master’s 
Degree in ‘Built Environment’ from the University of Greenwich. I have a strong affinity for 
both digital and physical designs. Starting my 3D journey with 3ds Max in 2002, I ‘played’ 
with other related tools such as Maya and Combustion (before its demise). I have since 
worked as an Architectural Designer, Visualisation Artist and Branding & Design Developer
As a believer in the role of effective communication between the designer and client, 
I have dedicated a considerable amount of time to making known the importance of 
visualisation in the design field, especially in architecture.</p>

</div>
</div>

<div class="row datar">
<div class="col-sm-12 title">
<h1>Featured</h1>
<form action="">
<input type="text" placeholder="Search featured">
<input type="image" src="images/search.png" name="search">
</form>
</div>
<div class="col-sm-12">
<div class="col-sm-12">
<div class="col-sm-4">
<a href="">
<div class="bg_overlay"></div>
<div class="like"><i class="fa fa-heart-o"></i><br>50</div>
<div class="comment"><i class="fa fa-comment-o"></i><br>150</div>
<div class="post_title"><strong>This is demo title</strong><br><strong style="font-size:14px;">M.K Ghosh</strong></div>
<img src="images/item1.jpg"></a>
</div>
<div class="col-sm-4">
<a href="">
<div class="bg_overlay"></div>
<div class="like"><i class="fa fa-heart-o"></i><br>50</div>
<div class="comment"><i class="fa fa-comment-o"></i><br>150</div>
<div class="post_title"><strong>This is demo title</strong><br><strong style="font-size:14px;">M.K Ghosh</strong></div>
<img src="images/item2.jpg"></a>
</div>
<div class="col-sm-4">
<a href="">
<div class="bg_overlay"></div>
<div class="like"><i class="fa fa-heart-o"></i><br>50</div>
<div class="comment"><i class="fa fa-comment-o"></i><br>150</div>
<div class="post_title"><strong>This is demo title</strong><br><strong style="font-size:14px;">M.K Ghosh</strong></div>
<img src="images/item1.jpg"></a>
</div>
<div class="col-sm-4">
<a href="">
<div class="bg_overlay"></div>
<div class="like"><i class="fa fa-heart-o"></i><br>50</div>
<div class="comment"><i class="fa fa-comment-o"></i><br>150</div>
<div class="post_title"><strong>This is demo title</strong><br><strong style="font-size:14px;">M.K Ghosh</strong></div>
<img src="images/item2.jpg"></a>
</div>
<div class="col-sm-4">
<a href="">
<div class="bg_overlay"></div>
<div class="like"><i class="fa fa-heart-o"></i><br>50</div>
<div class="comment"><i class="fa fa-comment-o"></i><br>150</div>
<div class="post_title"><strong>This is demo title</strong><br><strong style="font-size:14px;">M.K Ghosh</strong></div>
<img src="images/item1.jpg"></a>
</div>
<div class="col-sm-4">
<a href="">
<div class="bg_overlay"></div>
<div class="like"><i class="fa fa-heart-o"></i><br>50</div>
<div class="comment"><i class="fa fa-comment-o"></i><br>150</div>
<div class="post_title"><strong>This is demo title</strong><br><strong style="font-size:14px;">M.K Ghosh</strong></div>
<img src="images/item2.jpg"></a>
</div>
</div>
<a class="view_more" href="">View More</a>
</div>
</div>


</div>

<?php include_once('footer.php');?>
<footer>
<div class="f_top"></div>
<div class="f_content">
<div class="container">
  <div class="row">
    <div class="col-sm-3 fs_1">
    <img src="images/footer_logo.png" />
	<h5 style="padding-top:10px;"><b>About CG Africa page ok</b></h5>
    <p>CG Africa is an online portal providing quality resources for visualization artist, 3D modelers, animators and industry enthusiasts in Africa and across the globe.</p>
    <p>CG Africa is dedicated to advancing the course of computer graphics via informative trainings, web based classes and its diverse user community.
Contact: info@cgafrica.com</p>    
    </div>
    <div class="col-sm-3 fs_2">
    	<h5><b>Contacts:</b></h5>
        <ul>
        <li><a href="">Gallery</a></li>
        <li><a href="">Features</a></li>
        <li><a href="">Privacy</a></li>
        <li><a href="">Terms & Conditions</a></li>
        </ul>
    </div>
    <div class="col-sm-3 fs_3">
	<h5><b>Connect with us:</b></h5>
        <ul>
        <li><a href=""><img src="images/facebook.png"> Facebook</a></li>
        <li><a href=""><img src="images/twitter.png"> Twitter</a></li>
        <li><a href=""><img src="images/googlep.png"> Google+</a></li>
        <li><a href=""><img src="images/instagram.png"> Instagram</a></li>
        </ul>
		<h5><b>Join our newsletter</b></h5>
		<form class="newsletter">
		<input type="text" placeholder="Your email address"><input type="submit" value="Join">
		</form>
    </div>
    <div class="col-sm-3 fs_4">
    <img width="66" src="images/copyright.png" />
    <p style="padding-top:15px;">&copy;2015 | Ostudiolabs
All rights reserved. No part of this website may be reproduced unless for personal use without prior written permission from  <a href="http://www.ostudiolabs.com/" target="_blank">Ostudiolabs</a>.</p>
    </div>
  </div>
  </div>
</footer>

<!-- jQuery --> 
<script src="js/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript -->
<link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<link rel="stylesheet"  href="css/lightslider.css"/>
 
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-switch.js"></script>
<script src="js/highlight.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/lightslider.js"></script>
    <script>
    	 $(document).ready(function() {
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:5,
                slideMargin: 10,
				galleryMargin: 2,
				thumbMargin: 2,
                speed:500,
				pause: 10000,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
    </script>
 
<script type="text/javascript">
$(document).ready(function(){
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

if (!isChrome) {
$('.scroll-pane').jScrollPane();
}
$('.pname').click(function(){
$('#pname,.pnameextra').hide();
$('#epname').show().focus();
$('.spname,.rpname').show();

});

$('.spname,.rpname').click(function(){
$('#pname,.pnameextra').show();
$('#epname,.spname,.rpname').hide();
});

$('.pskills').click(function(){
$('#pskills,.pskillsextra').hide();
$('#epskills').show().focus();
$('.spskills,.rpskills').show();
});

$('.spskills,.rpskills').click(function(){
$('#pskills,.pskillsextra').show();
$('#epskills,.spskills,.rpskills').hide();
});

$('.plocation').click(function(){
$('#plocation,.plocationextra').hide();
$('#eplocation').show().focus();
$('.splocation,.rplocation').show();
});

$('.splocation,.rplocation').click(function(){
$('#plocation,.plocationextra').show();
$('#eplocation,.splocation,.rplocation').hide();
});

$('.pdes').click(function(){
$('#pdes,.pdesextra').hide();
$('#epdes').show().focus();
$('.spdes,.rpdes').show();
});

$('.spdes,.rpdes').click(function(){
$('#pdes,.pdesextra').show();
$('#epdes,.spdes,.rpdes').hide();
});

$('.password').keypress(function(e) { 
                var s = String.fromCharCode( e.which );

                if((s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) ||
                   (s.toUpperCase() !== s && s.toLowerCase() === s && e.shiftKey)){
                    if($('#capsalert').length < 1) $(this).after('<span id="capsalert">CAPS lock <i class="fa fa-caret-right"></i></span>');
                } else {
                    if($('#capsalert').length > 0 ) $('#capsalert').remove();
                }
            });

			
$("#qform").click(function() {
    var val=$(this).val();
	if(val=='Type a question here')
	$(this).val("");
});

$("#qform").blur(function() {
    var val=$(this).val();
	if(val=='')
   $(this).val("Type a question here");
});

$(".semail").click(function() {
    var val=$(this).val();
	if(val=='Sign in with email address')
	$(this).val("");
});

$(".semail").blur(function() {
    var val=$(this).val();
	if(val=='')
   $(this).val("Sign in with email address");
});

$(".semail2").click(function() {
    var val=$(this).val();
	if(val=='Enter your email address')
	$(this).val("");
});

$(".semail2").blur(function() {
    var val=$(this).val();
	if(val=='')
   $(this).val("Enter your email address");
});

$(".semail3").click(function() {
    var val=$(this).val();
	if(val=='Re-enter your email address')
	$(this).val("");
});

$(".semail3").blur(function() {
    var val=$(this).val();
	if(val=='')
   $(this).val("Re-enter your email address");
});

$(".password").click(function() {
    var val=$(this).val();
	if(val=='Enter your password')
	{
		$(this).attr('type','password');
		$(this).val("");
	}
});

$(".password").blur(function() {
    var val=$(this).val();
	if(val=='')
	{
		$(this).attr('type','text');
		$(this).val("Enter your password");
   }
});

$(".password2").click(function() {
    var val=$(this).val();
	if(val=='Enter your password')
	{
		$(this).attr('type','password');
		$(this).val("");
	}
});

$(".password2").blur(function() {
    var val=$(this).val();
	if(val=='')
	{
		$(this).attr('type','text');
		$(this).val("Enter your password");
   }
});

$(".password3").click(function() {
    var val=$(this).val();
	if(val=='Re-enter your password')
	{
		$(this).attr('type','password');
		$(this).val("");
	}
});

$(".password3").blur(function() {
    var val=$(this).val();
	if(val=='')
	{
		$(this).attr('type','text');
		$(this).val("Re-enter your password");
   }
});

$(".selection").click(function() {
    var sclass=$(this).attr('class');
	var id=$(this).attr('alt');
	if(sclass=='selection coff')
	{
		$(this).removeClass('coff').addClass('con');
		$('#'+id).val(1);
	}
	else
	{
		$(this).removeClass('con').addClass('coff');
		$('#'+id).val(0);
	}
});

$(".fclick").click(function() {
    var val=$(this).val();
	var placeholder=$(this).attr('data-placeholder');
	
	if(val==placeholder)
	$(this).val("");
});

$(".fclick").blur(function() {
    var val=$(this).val();
	var placeholder=$(this).attr('data-placeholder');
	if(val=='')
   $(this).val(placeholder);
});			

});
</script>
</body>
</html>
